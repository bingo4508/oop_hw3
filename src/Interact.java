import java.util.Scanner;

public class Interact{
	private String input;
	private POODirectory p;
	private POOArticle a;
	private POOBoard cbrd;
	private boolean InArticle;
	private boolean InBoard;
	private boolean Append;
	public static String manual = "--------------------- MANUAL ------------------------\n"+
                                "In Directory -\n"+
                                "      addB : add board (Enter board name after ':')\n"+
                                "      addD : add directory (Enter dir name after ':')\n"+
                                "      addS : add split line\n"+
                                "      show : show items in the directory\n"+
                                "      cd (#) : enter (#) board or directory\n"+
                                "In Board -\n"+
                                "      add : add article (Edit article afterwards,':wq' in 1 line to finish')\n"+
                                "      show (#) : show (#) article's full content\n"+
                                "      list (#) : show (#) article's brief information\n"+
                                "In Article -\n"+
                                "      push : push article (Enter string after ':')\n"+
                                "      boo : boo article (Enter string after ':')\n"+
                                "      arrow : give arrow (Enter string after ':')\n"+
                                "Common - \n"+
                                "      back : back to previous location\n" +
                                "      exit : exit program directly\n"+
				"      ? : show manual\n";

	private void clear(){
		System.out.print("\u001b[2J"+"\u001b[H");
	}
        private void presskey(){
                System.out.println("\n---Press Enter to continue---");
                try{
                        System.in.read();
                        clear();
                }catch(Exception e){}
        }
        private void AddBoard(String bname,POODirectory dir){
                dir.add(new POOBoard(bname,dir));
        }
        private void AddDir(String dname,POODirectory dir){
                dir.add(new POODirectory(dname,dir));
        }
        private void AddArticle(String title,String author,String content,POOBoard brd){
                brd.add(new POOArticle(title,author,content));
        }
	private void Edit(){
		clear();
		Scanner s = new Scanner(System.in);
		String title,author,content="",tmp;
		System.out.print("#Title : ");
		title = s.nextLine();
		System.out.print("#Author : ");
		author = s.nextLine();
		System.out.println("#Content :");
		while(true){
			tmp = s.nextLine();
			if(tmp.equals(":wq")){
				AddArticle(title,author,content,cbrd);
				break;
			}else content = content.concat(tmp+'\n');
		}
	}
	public void interact(POODirectory cdir){
		Scanner s = new Scanner(System.in);
		int n1=0,n2=0,nlen=0;
		while(true){
			if(Append == true) Append = false;
			else{
				clear();
				if(InBoard == true) cbrd.show();
				else if(InArticle == true) a.show();
				else cdir.show();
			}
			System.out.print("->");
			input = s.nextLine();
			String[] split = input.split(" ");
			nlen = split.length;
	                try {
			     if(nlen == 2)  n1  = Integer.parseInt(split[1]);
			     else if(nlen == 3){
				   n1 = Integer.parseInt(split[1]);
				   n2 = Integer.parseInt(split[2]);
			     }
		        }
		        catch(NumberFormatException e) {
		                System.out.println("Arguments must be number!");
				Append = true;
				continue;
		        }
			if(split[0].equals("?")){
				clear();
				System.out.print(manual);
				presskey();	
			}else if(split[0].equals("back")){
				if(InArticle == true){
					InArticle = false;
					InBoard = true;
					cbrd.show();
				}else{
					if(InBoard == true){
						InBoard = false;
					}else{
						p = cdir.getparent();
						if(p != null) cdir = p;
					}
				}
			}else if(split[0].equals("exit")){
				break;	
			}else if(InBoard == true){
				if(split[0].equals("show")||split[0].equals("list")){
					a = cbrd.article(n1);
					if(a != null && nlen > 1){
					    if(split[0].equals("show")){
						InBoard = false;
						InArticle = true;
					    }else if(split[0].equals("list")){
						Append = true;
						a.list();
					    }
					}else Append = true;
				}else if(split[0].equals("add")){
					Edit();
				}else if(split[0].equals("del")){
				      if(nlen > 1){
					 cbrd.del(n1);
				      }else Append = true;
				}else if(split[0].equals("move")){
				      if(nlen > 2){
					 cbrd.move(n1,n2);
				      }else Append = true;
				}else Append = true;
			}else if(InArticle == true){
				if(split[0].equals("push")){
					System.out.print(":");
					a.push(s.nextLine());
				}else if(split[0].equals("boo")){
					System.out.print(":");
					a.boo(s.nextLine());
				}else if(split[0].equals("arrow")){
					System.out.print(":");
					a.arrow(s.nextLine());
				}else Append = true;
			}else{
				if(split[0].equals("addB")){		//board
					System.out.print(":");
					AddBoard(s.nextLine(),cdir);
				}else if(split[0].equals("addD")){	//directory
					System.out.print(":");
					AddDir(s.nextLine(),cdir);
				}else if(split[0].equals("addS")){	//split
					cdir.add_split();
				}else if(split[0].equals("del")){
					System.out.printf("nlen=%d\n",nlen);
				      if(nlen > 1){
					 cdir.del(n1);
				      }else Append = true;
				}else if(split[0].equals("move")){
				      if(nlen > 2){
					 cdir.move(n1,n2);
				      }else Append = true;
				}else if(split[0].equals("cd")){
				      if(nlen >1){
					 if(cdir.item(n1) instanceof POODirectory){
						cdir = (POODirectory)cdir.item(n1);
					 }else if(cdir.item(n1) instanceof POOBoard){
						cbrd = (POOBoard)cdir.item(n1);
						InBoard = true;
					 }else Append = true;
				      }else Append = true;
				}else Append = true;
			}
		}
	}
}
