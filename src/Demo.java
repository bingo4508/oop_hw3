class Demo{
	private static void clear(){
		System.out.print("\u001b[2J"+"\u001b[H");     //cls & back 2 1st line
	}
	private static void sleep(double sec){
		try {
		    Thread.sleep((int)(sec*1000));
		} catch(InterruptedException ex) {
		    Thread.currentThread().interrupt();
		}
	}
	private static void presskey(){
		System.out.println("\n---Press Enter to continue---");  
		try{
			System.in.read();
			clear();
		}catch(Exception e){}
	} 
	private static void AddBoard(String bname,POODirectory dir){
		dir.add(new POOBoard(bname,dir));
	}
	private static void AddDir(String dname,POODirectory dir){
		dir.add(new POODirectory(dname,dir));
	}
	private static void AddArticle(String title,String author,String content,POOBoard brd){
		brd.add(new POOArticle(title,author,content));
	}
	public static void main(String[] args){
		POODirectory root,cdir;
		POOBoard cbrd;
		Interact usr = new Interact();;
		root = new POODirectory("My Favorite",null);

		clear();	
		System.out.print(Interact.manual);	//Print manual
		presskey();

		if(args.length == 0){
			usr.interact(root);
		}else if(args[0].equals("-a")){			//auto demo
			cdir = root;				//cdir : current directory;
			cdir.show();
			System.out.print("->addB\n:NTU\n->addB\n:NCCU\n"+
					 "->addB\n:joke\n->addB\n:StupidClown\n"+
					 "->addS\n->addD\n:Dir1\n->addD\n:Dir2\n->addD\n:Dir3\n"
					);
			presskey();

			AddBoard("NTU",cdir);			//cbrd : current board
			AddBoard("NCCU",cdir);
			AddBoard("joke",cdir);
			AddBoard("StupidClown",cdir);
			cdir.add_split();
			AddDir("Dir1",cdir);
			AddDir("Dir2",cdir);
			AddDir("Dir3",cdir);
			cdir.show();
			System.out.print("->cd 2\n");
			presskey();

			cbrd = (POOBoard)cdir.item(2);	//joke
			cbrd.show();
			presskey();
			
			AddArticle("Why apple isn't orange?","Leo","Because apple is not orage.",cbrd);
			AddArticle("OOP is so easy~","HTLin","Hahaha",cbrd);
			AddArticle("QQQQQQ","Wu-JiaLun","TTTTTT",cbrd);
			AddArticle("Don't Swear At Other Drivers!",
				   "John",
			"Eddie was driving down the road and a met a car coming the other way.\n"+
			"Although there was room to pass easily, Eddie forced the oncoming car to slow down\n"+
			"and wound down his window and shouted 'Pig'.\n"+
			"The other driver looked in his rear view mirror and swore at Eddie.\n"+
			"Then his car hit the pig.",cbrd);
			AddArticle("CSIE","K","Q",cbrd);
			cbrd.show();
			System.out.print("->show 3\n");
			presskey();

			cbrd.article(3).show();
			System.out.print("->push\n:Good Job!\n->boo\n:Lousy!\n->boo\n:WTF...\n"+
					 "->arrow\n:So so..\n");
			presskey();
				
			cbrd.article(3).push("Good Job!");
			cbrd.article(3).boo("Lousy!");
			cbrd.article(3).boo("WTF...");
			cbrd.article(3).arrow("So so..");
			cbrd.article(3).show();
			System.out.print("->back");
			presskey();

			cbrd.show();
			System.out.print("->list 0\n");
			presskey();

			cbrd.show();
			cbrd.article(0).list();
			System.out.print("->del 2\n");
			presskey();
			
			cbrd.del(2);
			cbrd.show();
			System.out.print("->move 0 3\n");
			presskey();

			cbrd.move(0,3);
			cbrd.show();
			System.out.print("->back\n");
			presskey();

			cdir.show();
			System.out.print("->del 4\n");
			presskey();
	
			cdir.del(4);
			cdir.show();
			System.out.print("->move 1 2\n");
			presskey();

			cdir.move(1,2);
			cdir.show();
			System.out.print("->exit\n");
			presskey();
		}
	}
}
