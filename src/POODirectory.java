public class POODirectory{
	private String name;
	private Object[] items;
	private short item_c;
	private POODirectory parent;

	public POODirectory(String name,POODirectory parent){
		this.name = "/"+new String(name);
		this.parent = parent;
		items = new Object[1024];
	}
	public void add(POOBoard board){
		if(item_c == 1024){
			System.out.println("Add board fail! Directory is full!");
		}else{
			items[item_c++] = board;
		}
	}
	public void add(POODirectory dir){
		if(item_c == 1024){
			System.out.println("Add dir fail! Directory is full!");
		}else{
			items[item_c++] = dir;
		}
	}
	public void add_split(){
		if(item_c == 1024){
			System.out.println("Add split line fail! Directory is full!");
		}else{
			items[item_c++] = "----------------------";
		}
	}
	public void del(int pos){
		if(pos<0 || pos >= item_c){
                        //System.out.println("Del item fail! Wrong pos!");
                }else{
			for(int i=pos+1;i<item_c;i++)
				items[i-1] = items[i];
		        item_c--;
		}
	}
        public void move(int src,int dest){
                if((src<0 || src >= item_c)||(dest<0 || dest >= item_c)){
                        //System.out.println("Move item fail! Wrong src or dest!");
                }else{
                        Object tmp = items[src];
                        if(src < dest){
                                for(int i=src+1;i<=dest;i++)
                                        items[i-1] = items[i];
                                items[dest] = tmp;
                        }else if(src > dest){
                                for(int i=src-1;i>=dest;i--)
                                        items[i+1] = items[i];
                                items[dest] = tmp;
                        }
                }
        }
        public Object item(int pos){
                if(pos < 0  || pos >= item_c || item_c == 0){
                        //System.out.println("Select item fail! Wrong pos!");
                        return null;
                }else if(items[pos] instanceof String){
	                //System.out.println("Select item fail! Not Board or Dir!");
                        return null;
		}else{
                        return items[pos];
                }
        }
	public POODirectory getparent(){
		return this.parent;
	}
        public int length(){
                return (int)item_c;
        }
        public void show(){
		System.out.printf("#Directory <%s> :  (%d items)\n",this.name, length());
                for(int i=0;i<item_c;i++)
                        System.out.printf("\t%d. %s\n",i,items[i].toString());
		System.out.println("-----------------------------------------------");
        }
	public String toString(){
		return this.name;
	}
}
