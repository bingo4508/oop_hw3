public class POOArticle{
	private static short ID_c = 0;
	private short ID;
	private String title;
	private String author;
	private String content;
	private short eval;
	private String[] evalm;
	private short evalc;
	private static final byte MAXEVAL = 50;
	
	public POOArticle(String title,String author,String content){
		if(ID_c<1024){
			this.title = new String(title);
			this.author = new String(author);
			this.content = new String(content);
			this.ID = ID_c++;
			this.evalm = new String[1024];
		}
	}

	public void push(String str){
		eval++;
		if(evalc ==1024){
			System.out.println("Push fail ! Evaluation full!");
		}else if(str.length() <= MAXEVAL){
			evalm[evalc++] = "PUSH : "+str;
		}
	}
	public void boo(String str){
		eval--;
		if(evalc ==1024){
			System.out.println("Boo fail ! Evaluation full!");
		}else if(str.length() <= MAXEVAL){
			evalm[evalc++] = "BOO : "+str;
		}
	}
	public void arrow(String str){
		if(evalc ==1024){
			System.out.println("Arrow fail ! Evaluation full!");
		}else if(str.length() <= MAXEVAL){
			evalm[evalc++] = "~> : "+str;
		}
	}
	public void show(){
		System.out.printf("#ID : %04d\n",this.ID);
		System.out.printf("#Title : %s\n",this.title);
		System.out.printf("#Author : %s\n",this.author);
		System.out.printf("#Content :\n%s\n",this.content);
		System.out.println("...........................................");
		for(int i=0;i<evalc;i++)
			System.out.printf("%s\n",this.evalm[i]);
		if(evalc>0)  System.out.println("...........................................");
	}
	public void list(){
		System.out.printf("\n#ID : %04d\n",this.ID);
		System.out.printf("#Title : %s\n",this.title);
		System.out.printf("#Author : %s\n",this.author);
		System.out.printf("#Evaluation : %d\n\n",this.eval);
	}
	public String gettitle(){
		return this.title;
	}
}
