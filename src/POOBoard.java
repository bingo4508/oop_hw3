public class POOBoard{
	private String name;
	private POOArticle[] articles;
	private short art_c;
	private POODirectory parent;

	public POOBoard(String name, POODirectory parent){
		this.name = new String(name);
		articles = new POOArticle[1024];
		this.parent = parent;
	}
	public void add(POOArticle article){
		if(art_c == 1024){
			System.out.println("Add article fail! Board is full!");
		}else{
			articles[art_c++] = article;
		}
	}
	public void del(int pos){
		if(pos<0 || pos >= art_c){
			//System.out.println("Del article fail! Wrong pos!");
		}else{
			for(int i=pos+1;i<art_c;i++)
				articles[i-1] = articles[i];
			art_c--;
		}
	}
	public void move(int src,int dest){
		if((src<0 || src >= art_c)||(dest<0 || dest >= art_c)){
			//System.out.println("Move article fail! Wrong src or dest!");
		}else{
			POOArticle tmp = articles[src];
			if(src < dest){
				for(int i=src+1;i<=dest;i++)
					articles[i-1] = articles[i];
				articles[dest] = tmp;
			}else if(src > dest){
				for(int i=src-1;i>=dest;i--)
					articles[i+1] = articles[i];
				articles[dest] = tmp;
			}
		}
	}
	public POOArticle article(int pos){
		if(pos < 0  || pos >= art_c || art_c == 0){
			//System.out.println("Select article fail! Wring pos!");
			return null;
		}else{
			return articles[pos];
		}
	}
	public POODirectory getparent(){
		return this.parent;
	}
	public int length(){
		return (int)art_c;
	}
	public void show(){
		System.out.printf("#Board <%s> : (%d articles)\n",this.name,length());
		for(int i=0;i<art_c;i++)
			System.out.printf("\t%d. %s\n",i,articles[i].gettitle());
		System.out.println("-----------------------------------------------");
	}
	public String toString(){
		return this.name;
	}
}
